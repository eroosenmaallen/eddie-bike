# Eddie-Bike

This is a replacement exercise bike display module. It supports a one-pulse-per-rotation cadence sensor, and has support (but currently no use) for a front-pabnel button.


## The Hardware

This was prototyped on an Arduino Uno and a bit of breadboard; the finished project uses an Arduino Nano and a permanent perfboard curcuit.

The main bits are the OLED display and the cadence sensor connection; there is also a front-panel button connected (it was there, so why not?) that I have ideas about.

The cadence sensor is built into the bike, and acts as a reed switch - at a certain point in the rotation, a magnet swings by and the switch closes. I paired that with an internal pullup to detect the cadence.

The OG display module had an auto power-off, but I've added a front-panel toggle switch -- learning AVR power management might be a future stage of the project.

The "production" circuit is laid out on perfboard, adapted from the original breadboard design. The layout was designed in Fritzing, with some adaptations to make it reasonable in both software and real life (eg. one-wire-per-hole on the perfboard, so the real life version has some nasty big solder busses, but in Fritzing it looks mostly pretty tidy).

For the current build, the nano is attached via jumper wires with Dupont headers, but I'm not 100% happy with the connection; during early testing it came unplugged quite often, to the point I removed the shroud entirely on one side and just jammed the bare sockets onto the Nano's pins. If it wiggles loose now, my next plan is to remove the pin headers, and solder the wire harness directly into the nano. It's a buttload of de/soldering, but it might be the only way to get a good connection.


## The Code

The cadence-counting code is pretty simple; I use an interrupt handler and timer to detect and debounce pulses from the cadence sensor, then set a flag with the timestamp. In the loop(), I use that flag to calculate the current RPM, then I calculate a moving average from that. 

Speed and distance are calculated directly from the RPM; I use an arbitrary distance per revolution (presently 6m per rev) arrived at with napkin math and some trial-and-error to get plausible numbers. Each time a cadence pulse is recorded, I add `METERS_PER_REV` to the total distance; speed is `METERS_PER_REV * rpm`, then multiply by `60 / 1000` to get km/h.

Later in the loop we do some timing to detect a pause in pedalling, to pause the elapsed time display and gracefully decay the RPM.

The rest of the code is related to the display, choosing what to show and actually drawing it on the OLED.


## Progress and Plans

Like a carpenter's house, an arduino nerd's custom DIY display is never done. I have some ideas for expansion, and the code could certainly use some cleanup.

* General code cleanup
* Display modes, "scan mode", use front-panel button to pick mode
* mph / kmh switch -- maybe long-press the button to switch?
* power management?
* nRF52/ESP32/other BLE controller for BLE/ANT+ connection to watch/phone

