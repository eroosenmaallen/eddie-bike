/** 
 *  @file
 *  Stationary bike counter
 *
 *  @author Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date   October 2022
 *
 *  Hardware Connections:
 *  A4  i2c-SDA
 *  A5  i2c-SCL
 *  D2  Cadence counter -- magnetic reed switch
 *  D3  Front Panel Button
 */

#define METERS_PER_REV 6.0

// setup the display
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


// Timers
#include "Timer.h"

Timer ridingTime;
Timer pulseTimer;
Timer idleTimer;
Timer screenTimer;
Timer decayTimer;

unsigned long dist = 0;

// Other configuration

// pin for the pedal sensor
#define PIN_PEDAL 2
// pin for the front panel button
#define PIN_PANEL 3

// EMA coefficient for RPM running total
#define COEFF     0.3


void setup() {
  Serial.begin(115200);


  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
  }

  displayStartup();

  Serial.println(F("Welcome to EddieBike!"));

  pinMode(PIN_PEDAL, INPUT_PULLUP);
  pinMode(PIN_PANEL, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PIN_PEDAL), countPulse, FALLING);
}

// holder for the time of the last measures pulse. Updated by pulse ISR, read in loop()
unsigned long lastPulse = 0;

// track our RPM
float rpm = 0.0;

bool panelWas = 1;

void loop() {
  bool updateScreen = false;

  bool panel = digitalRead(PIN_PANEL);
  if (panel != panelWas && !panel) {
    panelWas = panel;
    Serial.println("Panel button press");
  }
  else if (panel != panelWas) {
    panelWas = panel;
    Serial.println("Panel button release");
  }

  // if a pulse has been measured, EMA it into our speed
  unsigned long lp = lastPulse;  
  if (lp) {
    lastPulse = 0;
    float newRpm = 60000.0 / lp; // 60,000 ms = 1 minute
    decayTimer.start();

    dist += METERS_PER_REV;

    // if tracked rpm is zero, then just set it to the new calced value
    if (rpm == 0.0) {
      rpm = newRpm;
    }
    else {
      // if we already have a value, then EMA in the new one
      rpm = (COEFF * rpm) + ((1.0 - COEFF) * newRpm);
    }

    // start or unpause the riding timer
    if (ridingTime.state() == PAUSED) {
      ridingTime.resume();
    }
    else if (ridingTime.state() == STOPPED) {
      ridingTime.start();
      dist = 0;
    }

    Serial.print(F("pulse;"));

    Serial.print(F("time="));
    Serial.print(lp);
    Serial.print(',');

    Serial.print(F("newRpm="));
    Serial.print(newRpm, 2);
    Serial.print(',');

    Serial.print(F("rpm="));
    Serial.print(rpm, 2);
    Serial.print(',');

    Serial.println();
    
    updateScreen = true;
  }
  else if (rpm > 0.0
    && decayTimer.state() == RUNNING
    && decayTimer.read() > 1100) {
    rpm = 0.8 * rpm;
    if (rpm < 2.0)
      rpm = 0;
    
    Serial.print(F("paused; rpm decaying to "));
    Serial.println(rpm, 2);

    decayTimer.start();
  }


  // have we stopped pedalling?
  if ((pulseTimer.state() == RUNNING)
    && (pulseTimer.read() > (20 * 1000))) {
    Serial.println(F("Detected pause"));

    // zero out the speed
    rpm = 0.0;

    // stop the inter-pulse timer
    pulseTimer.stop();

    // pause the riding time, if running
    if (ridingTime.state() == RUNNING) {
      ridingTime.pause();
    }

    // start the idle timer
    idleTimer.start();
    
    updateScreen = true;
  }


  // if we've been idle for 5 minutes, assume we've stopped
  if (idleTimer.state() == RUNNING
    && idleTimer.read() > (5 * 60 * 1000)) {
    Serial.println(F("Stopped Riding"));
    ridingTime.stop();
    displayStartup();    
  }

  if (updateScreen
   || (screenTimer.state() == RUNNING 
    && screenTimer.read() > 250)) {
    displayFrame();
    // logFrame();
  }
}


/// ---

// When we detect a pulse, mark the time since the last pulse in `lastPulse`
// then (re-)start the timer
void countPulse() {
  unsigned long pulse = pulseTimer.read();
  
  // count the time between pulses, as long as it's 200ms+
  if (pulseTimer.state() == RUNNING && (pulse > 200)) {
    lastPulse = pulse;
    pulseTimer.stop();
  }

  pulseTimer.start();
}


// ---
// Serial functions

void logFrame() {
  Serial.print(F("rpm:"));
  Serial.print(rpm, 2);

  Serial.println();
}


// ---
// rendering functions

void displayStartup() {
  display.clearDisplay();

  display.setTextSize(2);                     // big 2:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);                    // Start at left-side-middle
  display.print(F("Welcome to EddieBike!"));

  display.display();
}

// render a full frame of riding data
void displayFrame() {
  display.clearDisplay();

  if (ridingTime.state() == RUNNING) {
    displayRpm();
    displaySpeed();
  }
  else if (ridingTime.state() == PAUSED) {
    displayPaused();
  }

  displayRidingTime();
  displayDistance();

  display.display();

  screenTimer.start();
}

void displayRpm() {
  display.setTextSize(2);                     // big 2:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);                     // Start at top-left corner
  if (rpm < 100)
    display.print(' ');
  if (rpm < 10)
    display.print(' ');
  display.print(rpm, 0);

  display.setCursor(36, 7);
  display.setTextSize(1);
  display.println(F("rpm"));
}

void displayRidingTime() {
  unsigned long elapsedS = ridingTime.read() / 1000;
  unsigned long state = ridingTime.state();

  // if paused, then blink the riding time at 1Hz50%
  if ((state == PAUSED) && ((millis() % 1000) < 500))
    return;

  int mm = floor(elapsedS / 60);
  int ss = elapsedS % 60;
  
  display.setTextWrap(false);
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(68, 0);                    // top-right corner
  if (mm < 10) display.print(' ');
  display.print(mm);
  display.print(':');
  if (ss < 10) display.print('0');
  display.print(ss);
}

void displayPaused() {
  display.setTextSize(2);                     // big 2:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,8);                     // Start at top-left corner
  display.println(F("paused"));
}

void displaySpeed() {
  float speed = rpm * (METERS_PER_REV * 60.0 / 1000.0);
  
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0, 16);                   // bottom-left corner
  if (speed < 10.0)
    display.print(' ');
  display.print(speed, 1);

  display.setCursor(48, 23);
  display.setTextSize(1);
  display.println(F("kmh"));
}

void displayDistance() {
  float distKm = dist / 1000.0;

  display.setTextWrap(false);
  display.setTextSize(2);                 // big 2:1 pixel scale
  display.setTextColor(SSD1306_WHITE);    // Draw white text

  display.setCursor(68, 16);               // right-justify in bottom half
  if (distKm < 10.0)
    display.print(' ');
  display.print(distKm, 1);

  display.setTextSize(1);
  display.setCursor(116, 23);
  display.print(F("km"));
}
